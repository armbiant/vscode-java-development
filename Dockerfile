FROM maven:3-ibm-semeru-17-focal

RUN curl -LO "https://github.com/google/google-java-format/releases/download/v1.14.0/google-java-format-1.14.0-all-deps.jar" && \
  mv google-java-format*.jar /usr/local/share/google-java-format.jar && \
  groupadd -g 1000 vscode && \
  useradd -u 1000 -g 1000 -s /bin/bash -m vscode

COPY google-java-format /usr/local/bin/

